# RUN #

* hadoop jar MFRJ.jar MFRJ <path_to_project> <number_of_columns> <conditions>

Sample:

* hadoop jar MFRJ.jar MFRJ /project 4 []

## Conditions ##

Conditions array contains set of condition JSON object:

* {"fact": <fact_name>, "operation": <operation>, "value": <value>, "type": <type>}

Sample: 

* {"fact": "count", "operation": ">", "value": "100", "type": "numeric"}

This is equivalent of SQL WHERE count > 100.

### Supported operations ###

* "="
* "!="
* ">"
* "<"
* ">="
* "<="

### Supported types ###

* numeric

Sample:

* hadoop jar MFRJ.jar MFRJ /project 4 [{"fact": "count", "operation": ">", "value": "100", "type": "numeric"}, {"fact": "count", "operation": "<", "value": "200", "type": "numeric"}]