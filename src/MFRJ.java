import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.RemoteIterator;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocatedFileStatus;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.apache.hadoop.mapreduce.lib.input.KeyValueTextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by Artem Ustimov on 21.02.2016.
 */
public class MFRJ extends Configured implements Tool {
    static final String DIMENSION_DIRECTORY = "/dim";
    static final String FOREIGN_KEY_FILENAME = "fks";

    static class MFRJMapper extends Mapper<Text, Text, IntWritable, Text> {
        private HashMap<String, ArrayList<String>> dimensionsHashMap = new HashMap<>();

        @Override
        protected void setup(Context context) throws IOException, InterruptedException {

            // Defining path to dimension directory
            final Configuration conf = context.getConfiguration();
            final String projectPath = conf.get(PROJECT_PATH);
            final FileSystem fileSystem = FileSystem.get(conf);
            final Path dimensionPathDirectory = new Path(projectPath + DIMENSION_DIRECTORY);
            final RemoteIterator<LocatedFileStatus> it = fileSystem.listFiles(dimensionPathDirectory, false);

            // No reason to continue if there are no dimension files
            if (it == null) {
                throw new FileNotFoundException("Dimension directory is empty.");
            }

            // Set up cache from dimension files content
            while (it.hasNext()) {
                final LocatedFileStatus file = it.next();
                final ArrayList<String> values = new ArrayList<>();
                final BufferedReader bufferedReader = new BufferedReader(
                        new InputStreamReader(fileSystem.open(file.getPath())));

                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    values.add(line);
                }
                bufferedReader.close();
                dimensionsHashMap.put(file.getPath().getName(), values);
            }
        }

        @Override
        public void map(final Text key, final Text value, final Context context)
                throws IOException, InterruptedException {

            // Transforming input line to json object
            final Gson gson = new Gson();
            final JsonObject jsonObject;
            final String currentFile = ((FileSplit)context.getInputSplit()).getPath().getName();

            if (currentFile.equals(FOREIGN_KEY_FILENAME)) {
                jsonObject = gson.fromJson(value.toString(), JsonObject.class);
            } else {
                jsonObject = new JsonObject();
                jsonObject.addProperty(currentFile, value.toString());
            }

            // Replacing Ids with values
            final Set<Map.Entry<String, JsonElement>> set = jsonObject.entrySet();
            for (Map.Entry<String, JsonElement> entry : set) {
                if (dimensionsHashMap.containsKey(entry.getKey()) && isNotIndexOutOfRange(entry)) {
                    jsonObject.addProperty(entry.getKey(), dimensionsHashMap.get(entry.getKey())
                            .get(entry.getValue().getAsInt()));
                }
            }

            final Text text = new Text(jsonObject.toString());
            final IntWritable intWritable = new IntWritable(Integer.parseInt(key.toString()));
            context.write(intWritable, text);
        }

        private boolean isNotIndexOutOfRange(Map.Entry<String, JsonElement> entry) {
            return dimensionsHashMap.get(entry.getKey()).size() > entry.getValue().getAsInt();
        }
    }

    static class MFRJReducer extends Reducer<IntWritable, Text, IntWritable, Text> {
        private int attributeCount;

        @Override
        protected void setup(Context context) throws IOException, InterruptedException {
            final Configuration conf = context.getConfiguration();
            attributeCount = Integer.parseInt(conf.get(ATTRIBUTE_COUNT));
        }

        @Override
        public void reduce(final IntWritable key, final Iterable<Text> values, final Context context)
                throws IOException, InterruptedException {

            final Gson gson = new Gson();
            final JsonObject result = new JsonObject();

            // Merging all values with the same key
            for (Text val : values) {
                final JsonObject jsonObject = gson.fromJson(val.toString(), JsonObject.class);
                final Set<Map.Entry<String, JsonElement>> set = jsonObject.entrySet();
                for (Map.Entry<String, JsonElement> entry : set) {
                    result.add(entry.getKey(), entry.getValue());
                }
            }

            if (result.entrySet().size() < attributeCount) {
                return;
            }

            final Text text = new Text(result.toString());
            context.write(key, text);
        }
    }

    @Override
    public int run(String[] args) throws Exception {

        final String projectPath = args[0];
        final Configuration conf = getConf();
        final Job job = Job.getInstance(conf, "MFRJ");

        job.setJarByClass(MFRJ.class);

        job.setMapperClass(MFRJMapper.class);
        job.setReducerClass(MFRJReducer.class);

        job.setOutputKeyClass(IntWritable.class);
        job.setOutputValueClass(Text.class);

        job.setInputFormatClass(KeyValueTextInputFormat.class);
        conf.set("mapreduce.input.keyvaluelinerecordreader.key.value.separator", "\t");

        job.setOutputFormatClass(TextOutputFormat.class);

        FileInputFormat.addInputPath(job, new Path(projectPath + "/input"));
        FileOutputFormat.setOutputPath(job, new Path(projectPath + "/output"));

        job.setNumReduceTasks(Integer.parseInt(args[2]));

        return job.waitForCompletion(true) ? 0 : 1;
    }

    static final String PROJECT_PATH = "project";
    static final String ATTRIBUTE_COUNT = "count";
    static final String REDUCE_TASKS = "reduce";

    public static void main(final String[] args) throws Exception {
        final Configuration conf = new Configuration();
        conf.set(PROJECT_PATH, args[0]);
        conf.set(ATTRIBUTE_COUNT, args[1]);
        conf.set(REDUCE_TASKS, args[2]);

        final int returnCode = ToolRunner.run(conf, new MFRJ(), args);
        System.exit(returnCode);
    }
}
